jQuery(function ($) {
	"use strict";

	/* ========================================================================= */
	/*	Page Preloader
	/* ========================================================================= */

	$(window).on('load', function () {
		$('.preloader').fadeOut(100);
	});


	// lazy load initialize
	const observer = lozad(); // lazy loads elements with default selector as ".lozad"
	observer.observe();



	/* ========================================================================= */
	/*	Magnific popup
	/* =========================================================================  */
	$('.image-popup').magnificPopup({
		type: 'image',
		removalDelay: 160, //delay removal by X to allow out-animation
		callbacks: {
			beforeOpen: function () {
				// just a hack that adds mfp-anim class to markup
				this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		},
		closeOnContentClick: true,
		midClick: true,
		fixedContentPos: false,
		fixedBgPos: true
	});
	/* ========================================================================= */
	/*	Portfolio Filtering Hook
	/* =========================================================================  */
  function filter(){
	var containerEl = document.querySelector('.shuffle-wrapper');
	if (containerEl) {
		var Shuffle = window.Shuffle;
		var myShuffle = new Shuffle(document.querySelector('.shuffle-wrapper'), {
			itemSelector: '.shuffle-item',
			buffer: 1
		});

		jQuery('input[name="shuffle-filter"]').on('change', function (evt) {
			var input = evt.currentTarget;
			if (input.checked) {
				myShuffle.filter(input.value);
			}
		});
	}}
	$(window).on('scroll', function () {
		filter();
	});
	/* ========================================================================= */
	/*	Testimonial Carousel
	/* =========================================================================  */

	$("#testimonials").slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000
	});

	/* ========================================================================= */
	/*	Animated section
	/* ========================================================================= */

	var wow = new WOW({
		offset: 100, // distance to the element when triggering the animation (default is 0)
		mobile: false // trigger animations on mobile devices (default is true)
	});

	var scrolled = false;
	$(window).on('scroll', function () {
		if (!scrolled) {
			scrolled = true;
			wow.init();
		}
	})


	/* ========================================================================= */
	/*	animation scroll js
	/* ========================================================================= */

	var html_body = $('html, body');
	$('nav a, .page-scroll').on('click', function () { //use page-scroll class in any HTML tag for scrolling
		if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				html_body.animate({
					scrollTop: target.offset().top - 70
				}, 1500, 'easeInOutExpo');
				return false;
			}
		}
	});

	// easeInOutExpo Declaration
	jQuery.extend(jQuery.easing, {
		easeInOutExpo: function (x, t, b, c, d) {
			if (t === 0) {
				return b;
			}
			if (t === d) {
				return b + c;
			}
			if ((t /= d / 2) < 1) {
				return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
			}
			return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
		}
	});

	/* ========================================================================= */
	/*	counter up
	/* ========================================================================= */
	function counter() {
		var oTop;
		if ($('.count').length !== 0) {
			oTop = $('.count').offset().top - window.innerHeight;
		}
		if ($(window).scrollTop() > oTop) {
			$('.count').each(function () {
				var $this = $(this),
					countTo = $this.attr('data-count');
				$({
					countNum: $this.text()
				}).animate({
					countNum: countTo
				}, {
					duration: 1000,
					easing: 'swing',
					step: function () {
						$this.text(Math.floor(this.countNum));
					},
					complete: function () {
						$this.text(this.countNum);
					}
				});
			});
		}
	}
	$(window).on('scroll', function () {
		counter();
	});

});

function main() {

	$(document).on("submit", "form.ajax-form", function(e) {
		console.log("holaaa send mail");
        e.preventDefault();
		var currentForm = $(this); // Get current form object
		
		$("[type=submit]", currentForm).attr("disabled", "disabled");
		
        let formData = $(this)
          .serializeArray()
          .map(
            function(x) {
              this[x.name] = x.value;
              return this;
            }.bind({})
		  )[0];

		axios({
			method: $(this).attr("method"),
			url: $(this).attr("action"),
			data: formData
		})
		.then(function(response) {
			console.log(response);
			var btnLabel = $("[type=submit]", currentForm).data("btn-label");
              $("[type=submit]", currentForm).removeAttr("disabled");
              $("[type=submit]", currentForm).html(btnLabel);
		})
		.catch(function(error) {
			console.log(response);
			var btnLabel = $("[type=submit]", currentForm).data("btn-label");
              $("[type=submit]", currentForm).removeAttr("disabled");
              $("[type=submit]", currentForm).html(btnLabel);
		});

        return false;
      });

	(function () {
	   'use strict';
	
	   

	   $('#myTimeline').verticalTimeline({
		startLeft: false,
		alternate: true,
		animate: "fade",
		arrows: false
	});

	   const graphContainer = document.getElementById("graph-container");
	
	   const renderMessage = (commit) => {
		 console.log(commit);
		
	  };
	
	   const options = {
		template: GitgraphJS.templateExtend("metro", {
		  colors: ["#66D9EF", "#A6E22E", "#AE81FF", "red"],
		  commit: {
			message: {
			  displayAuthor: false,
			  displayHash: false,
			  color: '#FFFFFF'
			},
			author: ''
		  },
		  commitDefaultOptions: {
			renderMessage: renderMessage
		  },renderMessage: renderMessage
		  // …
		}),
	  };
	
		// Instantiate the graph.
		const gitgraph = GitgraphJS.createGitgraph(graphContainer, options);
	
		// Simulate git commands with Gitgraph API.
		const master = gitgraph.branch("my life", );
		var hola = master.commit({subject: "[1988] Initial", author: " <Buenos Aires, Argentina>"});
		console.log(hola);
	
		const education = gitgraph.branch({name: "education"});
		education.commit({subject: '👨🏽‍🎓 Sun College College (Buenos Aires, Argentina)', tag: 'Graduate in Economy and Management of Organizations'});
		master.commit({ subject:'', author:'' });
		const work = gitgraph.branch({name:"work"});
		work.commit( { tag: "Spanish teacher ", subject: "👨🏽‍🏫 National Technology University" });
		education.commit({ subject:"👨🏽‍🎓 [2008] National Technology University (Buenos Aires, Argentina)",  tag: 'Advanced Technician in System Programming'});
		master.merge(education, ' ', ' ');
		work.commit({subject: "👨🏽‍💻 [2009/3 - 2012/6] S.U.T.E.R.H. (Buenos Aires, Argentina)", tag: 'Software developer '});
		master.commit({ subject:'', author:'' });
		const china = gitgraph.branch('china');
		china.commit({subject:"👨🏽‍🎓 [2012 - 2013] Zhengzhou University (Henan, China)",tag: "Chinese course"});
		china.commit({subject:"👨🏽‍🏫 Henan Agricultural University (Henan, China)",tag: "Spanish teacher"});
		// master.merge(china, '','');
	
		master.merge(china, ' ', ' ');
		
		work.commit({tag: 'Software developer', subject: "👨🏽‍💻 [2014/3 - 2015/02] Desarrollo Especial SRL"});
		work.commit({subject: "👨🏽‍💻 [2015/3 - Today] Universidad Metropolitana para la Educación y el Trabajo", tag: 'Full stack developer, DevOps, SRE'});
		//master.merge(education);
	
		// const aFeature = gitgraph.branch({name:"a-feature", parentBranch: master});
	   
		master.merge(work, ' ', ' ');
	}());
};

main();
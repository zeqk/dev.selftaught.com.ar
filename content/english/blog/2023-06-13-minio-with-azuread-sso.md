---
title: "MinIO with AAD SSO"
date: 2023-06-13T23:30:00+03:00
author: Ezequiel Perez
image_webp: images/blog/2023-06-13-minio-with-azuread-sso.webp
image: images/blog/2023-06-13-minio-with-azuread-sso.jpg
description : "How to configure MinIO to use Single Sign-On (SSO) with Azure AD"
---


I spent a lot of time researching how to configure MinIO to use Single Sign-On (SSO) with Azure AD, but the MiniO documentation doesn't explain exactly how to do it. 

https://min.io/docs/minio/linux/operations/external-iam/configure-openid-external-identity-management.html

After extensive research and a lot of trial and error, this is the way.

### Common variables

First set up the common variables we will use in the 3 steps

```sh
export AAD_TENANT_ID=00000000-0000-0000-0000-000000000000 # Azure AD Tenant ID
export AAD_SUBSCRIPTION_ID=00000000-0000-0000-0000-000000000000 # Azure AD Tenant ID
export AAD_GROUP="MiniO-Users"
export MINIO_DOMAIN=minio.mydomain.com
export MINIO_CONSOLE_DOMAIN=minio-console.mydomain.com
export MINIO_ROOT_USER=minioadmin
export MINIO_ROOT_PASSWORD=no123456
```

We also need to use these variables as terraform variables

```sh
export TF_VAR_tenant_id=$AAD_TENANT_ID
export TF_VAR_subscription_id=$AAD_SUBSCRIPTION_ID
export TF_VAR_aad_group=$AAD_GROUP
export TF_VAR_minio_console_domain=$MINIO_CONSOLE_DOMAIN
export TF_VAR_minio_server=$MINIO_ROOT_USER
export TF_VAR_minio_user=$MINIO_ROOT_USER
```

### 1. Azure AD Configuration

First, the configuration in Azure AD using Terraform

```terraform
variable "aad_group_name" {
  type = string
}

variable "minio_console_domain" {
  type = string
}

variable "tenant_id" {
  type = string
}

variable "subscription_id" {
  type = string
}

provider "azurerm" {
  features {
  }
  tenant_id                  = var.tenant_id
  subscription_id            = var.subscription_id
}

data "azuread_client_config" "current" {

}

# Create an AAD group
data "azuread_group" "group" {
  display_name = var.aad_group_name
}

# You will also need associate your users to this AAD group

# https://learn.microsoft.com/en-us/azure/active-directory/hybrid/how-to-connect-fed-group-claims
# Add a new Azure AD App registration
resource "azuread_application" "minio" {
  display_name = "MiniO"
  web {
    redirect_uris = ["https://${var.minio_console_domain}/oauth_callback"]
    implicit_grant {
      access_token_issuance_enabled = true
    }
  }
  
  optional_claims {
    access_token {
      name                  = "groups"
      additional_properties = ["sam_account_name"]
      essential             = true
    }
    id_token {
      name                  = "groups"
      additional_properties = ["sam_account_name"]
      essential             = true
    }
    saml2_token {
      name                  = "groups"
      additional_properties = ["sam_account_name"]
      essential             = true
    }
  }

  group_membership_claims = ["ApplicationGroup"]

  owners = [data.azuread_client_config.current.object_id]
}

# Add Enterprise Application
resource "azuread_service_principal" "minio" {
  application_id               = azuread_application.minio.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]
}

# Add credentials to Azure AD App registration
resource "azuread_application_password" "minio" {
  display_name          = "client_secret"
  application_object_id = azuread_application.minio.object_id
  end_date              = "2028-01-01T01:02:03Z"
}

resource "azuread_app_role_assignment" "minio_aadgroup" {
  app_role_id         = "00000000-0000-0000-0000-000000000000" # Don't change this
  principal_object_id = data.azuread_group.group.object_id
  resource_object_id  = azuread_service_principal.minio.object_id
}

output "client_id" {
  value = azuread_application.minio.application_id
}

output "client_secret" {
  value = azuread_application_password.minio.value
  sensitive   = true
}
```

Let's apply... 

```sh
terraform apply
```

Set the outputs to these variables...

```sh
export MINIO_AAD_CLIENT_ID=00000000-0000-0000-0000-000000000000 # The client_id of the previous step
export MINIO_AAD_SECRET_ID=aaaa # The client_secret of the previous step
```

### 2. Minio Setup

Now, set the MinIO instance using docker. We will use traefik for simplify the configuration


```yaml
version: "3.9"
services:
  minio:
    image: quay.io/minio/minio:RELEASE.2023-03-24T21-41-23Z
    container_name: minio
    restart: always
    command: server /data --console-address ":9090"
    environment:
      - MINIO_ROOT_USER=${MINIO_ROOT_USER}
      - MINIO_ROOT_PASSWORD=${MINIO_ROOT_PASSWORD}
      - MINIO_IDENTITY_OPENID_CONFIG_URL="https://login.microsoftonline.com/${AAD_TENANT_ID}/v2.0/.well-known/openid-configuration"
      - MINIO_IDENTITY_OPENID_CLIENT_ID=${MINIO_AAD_CLIENT_ID}
      - MINIO_IDENTITY_OPENID_CLIENT_SECRET=${MINIO_AAD_SECRET_ID}
      - MINIO_IDENTITY_OPENID_SCOPES="profile openid email"
      - MINIO_IDENTITY_OPENID_CLAIM_NAME="groups"
      - MINIO_IDENTITY_OPENID_REDIRECT_URI="https://${MINIO_CONSOLE_DOMAIN}/oauth_callback"
      - MINIO_IDENTITY_OPENID_DISPLAY_NAME="AzureAD"
      - MINIO_IDENTITY_OPENID_COMMENT="AzureAD" 
    volumes:
      - minio:/data
    labels:
      # Traefik enabled
      traefik.enable: "true"

      # Https redirection
      traefik.http.routers.minio-console-http.entrypoints: web
      traefik.http.routers.minio-console-http.rule: Host(`${MINIO_CONSOLE_DOMAIN}`)
      traefik.http.routers.minio-console-http.middlewares: "minio-console-https"
      traefik.http.middlewares.minio-console-https.redirectscheme.scheme: https

      # Console
      traefik.http.routers.minio-console.rule: Host(`${MINIO_CONSOLE_DOMAIN}`)
      traefik.http.routers.minio-console.entrypoints: websecure
      traefik.http.routers.minio-console.tls.certresolver: "lets-encrypt"
      traefik.http.routers.minio-console.service: "minio-console"
      traefik.http.services.minio-console.loadbalancer.server.port: "9090"

      # API
      traefik.http.routers.minio.rule: Host(`${MINIO_DOMAIN}`)
      traefik.http.routers.minio.entrypoints: websecure
      traefik.http.routers.minio.tls.certresolver: "lets-encrypt"
      traefik.http.routers.minio.service: "minio"
      traefik.http.services.minio.loadbalancer.server.port: "9000"


volumes:
  minio:
```

Let's start MinIO

```bash
docker-compose up -d
```

### 3. MiniO Configuration

We also need configure a policy in MinIO. The policy name has to match with AAD Group name

```terraform
variable "minio_server" {
  type = string
}

variable "minio_user" {
  type = string
}

variable "minio_password" {
  type = string
}

variable "aad_group_name" {
  type = string
}


terraform {
  required_providers {
    minio = {
      source  = "aminueza/minio"
      version = "1.14.0"
    }
  }
}

provider "minio" {
  # Configuration options
  minio_server   = var.minio_server
  minio_user     = var.minio_user
  minio_password = var.minio_password
  minio_ssl      = true
}

data "minio_iam_policy_document" "example_policy" {
  statement {
    sid = "1"
    actions = [
      "admin:*"
    ]
  }

  statement {
    sid = "2"
    actions = [
      "kms:*"
    ]
  }

  statement {
    sid = "3"
    actions = [
      "s3:*"
    ]
    resources = [
      "arn:aws:s3:::*",
    ]
  }
}

resource "minio_iam_policy" "aad_group_policy" {
  name   = var.aad_group_name
  policy = data.minio_iam_policy_document.example_policy.json
}

```

I hope this article is useful for you to configure your MinIO instance
---
title: "Cleaning up local Git branches that no longer exist on remote"
date: 2024-05-22T13:00:00+03:00
author: Ezequiel Perez
image_webp: images/blog/2024-05-22-cleaning-up-local-git-branches.webp
image: images/blog/2024-05-22-cleaning-up-local-git-branches.jpg
description : "Cleaning up local git branches using Bash or Powershell"
---

Over time, your local Git repository can become cluttered with branches that have been deleted from the remote repository. Cleaning up these branches can help maintain a tidy and organized development environment. In this post, I'll show you how to identify and delete these outdated branches using both PowerShell and Bash.

### Step 1: Update remote references

First, make sure your local Git references are up-to-date with the remote repository. This command will fetch updates and prune any branches that no longer exist on the remote:

```console
git fetch --prune
```

### Step 2: Identify and delete local branches

Next, you can identify and delete local branches that no longer exist on the remote in a single pipeline. This command filters out branches marked as `gone` and deletes them:

#### Using Bash

```bash
git branch -vv | grep 'gone]' | awk '{print $1}' | xargs git branch -D
```

Here's what each part of the command does:
- `git branch -vv` lists all local branches with their remote tracking status.
- `grep 'gone]'` filters the branches marked as `gone`.
- `awk '{print $1}'` extracts the branch names.
- `xargs git branch -D` deletes each identified branch forcefully.

#### Using Poweshell

```powershell
git branch -vv | Where-Object { $_ -match 'gone\]' } | ForEach-Object { $_.Trim().Split()[0] } | ForEach-Object { git branch -D $_ }
```

Here's what each part of the command does:
- `git branch -vv` lists all local branches with their remote tracking status.
- `Where-Object { $_ -match 'gone\]' }` filters the branches that are marked as `gone`.
- `ForEach-Object { $_.Trim().Split()[0] }` extracts the branch names.
- `ForEach-Object { git branch -D $_ }` deletes each identified branch forcefully.

### Full script

For convenience, you can combine the steps into a single script:

Bash

```bash
# Update remote references
git fetch --prune

# Identify and delete local branches that no longer exist on remote
git branch -vv | grep 'gone]' | awk '{print $1}' | xargs git branch -D
```

Powershell

```powershell
# Update remote references
git fetch --prune

# Identify and delete local branches that no longer exist on remote
git branch -vv | Where-Object { $_ -match 'gone\]' } | ForEach-Object { $_.Trim().Split()[0] } | ForEach-Object { git branch -D $_ }
```


## Conclusion

By running these scripts in PowerShell or Bash, you can keep your local Git repository clean and in sync with the remote repository, making your development process more efficient. Regularly cleaning up outdated branches helps maintain an organized and efficient workspace. Happy branching!